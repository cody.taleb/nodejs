#!/usr/bin/env nodejs

var http=require('http');
var os = require('os');

http.createServer(function (req,res) {
	res.writeHead(200, {"Content-type":"text/html"});
	res.write(
		"<!DOCTYPE html>\n" +
		"<html>\n" +
		"<head><title>Hello from nodejs</title></head>\n" +
		"<body>\n"+
		"<h1>Hello from "+os.hostname+"</h1>\n"+
		"</body>\n"
		);
		res.end();
		}).listen(80,'0.0.0.0');
		console.log('server running at http://localhost:80');
